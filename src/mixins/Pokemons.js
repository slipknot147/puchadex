import {computed} from "vue"
import store from "@/store"

export const pokemons = computed(() => (store.state.pokemons));

export const pokemonsListMap = (list) => {

	const value = list.map( ({name, url}) => {
		let id = url.split("/");
		id = id[id.length - 2];
		return {
			id,
			name,
			image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`
		}
	})
	store.commit("SET_STATE", {
		prop: "pokemons",
		value
	})
}