import { createStore } from 'vuex'

export default createStore({
  state: {
    pokemons: []
  },
  getters: {
  },
  mutations: {
    SET_STATE(state, {prop, value}){
      state[prop] = value;
    }
  },
  actions: {
  },
  modules: {
  }
})
